cyber_modules {
  key: "Canbus"
  value: {
    dag_files: "/apollo/modules/canbus/dag/canbus.dag"
  }
}
cyber_modules {
  key: "Control"
  value: {
    dag_files: "/apollo/modules/control/dag/control.dag"
  }
}
cyber_modules {
  key: "GPS"
  value: {
    dag_files: "/apollo/modules/drivers/gnss/dag/gnss.dag"
  }
}

cyber_modules {
  key: "Localization"
  value: {
    dag_files: "/apollo/modules/localization/dag/dag_streaming_rtk_localization.dag"
  }
}

modules {
  key: "RTK Recorder"
  value: {
    start_command: "nohup /apollo/scripts/rtk_recorder.sh start &"
    stop_command: "/apollo/scripts/rtk_recorder.sh stop"
    process_monitor_config {
      command_keywords: "record_play/rtk_recorder.py"
    }
    # RTK Recorder is not running in self-driving mode.
    required_for_safety: false
  }
}

modules {
  key: "RTK Player"
  value: {
    start_command: "nohup /apollo/scripts/rtk_player.sh start &"
    stop_command: "/apollo/scripts/rtk_player.sh stop"
    process_monitor_config {
      command_keywords: "record_play/rtk_player.py"
    }
  }
}

modules {
  key: "Data Recorder"
  value: {
    start_command: "/apollo/scripts/record_bag.py --start --all --split_duration 10s"
    stop_command: "/apollo/scripts/record_bag.py --stop"
    process_monitor_config {
      command_keywords: "cyber_recorder"
    }
  }
}

cyber_modules {
  key: "Perception"
  value: {
    dag_files: "/apollo/modules/perception/production/dag/dag_streaming_perception.dag"
  }
}

cyber_modules {
  key: "PerceptionCamera"
  value: {
    dag_files: "/apollo/modules/perception/production/dag/dag_streaming_perception_camera.dag"
  }
}


cyber_modules {
  key: "Prediction"
  value: {
    dag_files: "/apollo/modules/prediction/dag/prediction.dag"
  }
}

cyber_modules {
  key: "Planning"
  value: {
    dag_files: "/apollo/modules/planning/dag/planning.dag"
  }
}

cyber_modules {
  key: "Routing"
  value: {
    dag_files: "/apollo/modules/routing/dag/routing.dag"
  }
}

cyber_modules {
  key: "Transform"
  value: {
    dag_files: "/apollo/modules/transform/dag/static_transform.dag"
  }
}

cyber_modules {
  key: "Camera"
  value: {
    dag_files: "/apollo/modules/drivers/camera/dag/camera.dag"
  }
}

monitored_components {
  key: "Localization"
  value: {
    # Special LocalizationMonitor.
  }
}

monitored_components {
  key: "GPS"
  value: {
    # Special LocalizationMonitor.
  }
}

#monitored_components {
#  key: "Data Recorder"
#  value: {
#    process {
#      command_keywords: "cyber_recorder"
#    }
#    resource {
#      disk_spaces {
#        # For logs.
#        path: "/apollo/data"
#        insufficient_space_warning: 8
#        insufficient_space_error: 2
#      }
#      disk_spaces {
#        # For records.
#        path: "/media/apollo/internal_nvme"
#        insufficient_space_warning: 128
#        insufficient_space_error: 32
#      }
#    }
#  }
#}


monitored_components {
  key: "Ouster"
  value: {
    channel {
      name: "/apollo/sensor/lidar128/compensator/PointCloud2"
    }
  }
}
