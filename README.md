![logo](docs/oscar/images/main_page/lexus_rx_sl_2.png)

# OSCAR (Open-Source CAR)

Проект OSCAR -- это открытая программно-аппаратная платформа для создания беспилотного автомобиля [4-го уровня автономности](https://en.wikipedia.org/wiki/Autonomous_car#Levels_of_driving_automation) на базе проекта [apollo.auto](https://apollo.auto) от [Baidu](https://www.baidu.com/)


## Документация

[Иснтрукция по установке и сборке проекта](docs/oscar/README.md)

[Описание программно-аппаратной платформы](docs/oscar/soft_hard_description.md)

[Работа с симулятором](docs/oscar/lgsvl_simulator.md)


## Сообщество

В рамках проекта oscar ежегодно проводится **Хакатон Беспилотный автомобиль StarLine**, где участникам предоставлятся возможность разработать и представить решения передовых задач, стоящих перед исследователями в области автономного транспорта и искусственного интеллекта России и мира. Больше подробностей о регламенте можно найти на [официальной странице](https://robofinist.ru/event/info/short/id/339) и на [официальном репозитории](https://gitlab.com/starline/hackathon_kobuki) хакатона.

Для амбициозных студентов и активных специалистов из Open Source сообщетсва, которые ищут возможность участия в проекте, у нас имеется [список актуальных задач проекта](docs/oscar/task_for_students.md).


## Контакты

С нами можно связаться по почте **smartcar@starline.ru**.

А еще у нас есть [Twitter](https://twitter.com/starline_oscar) и [YouTube](https://www.youtube.com/channel/UC1ZPtOXu7cr4HsQKRpElDnA).
